FROM debian:bullseye-slim
LABEL authors="Tom Sievers <t.sievers@twizzel.net>"
LABEL description="Debian based image to perform generic CI tests / operations on projects."

# Install cron
RUN apt-get update
RUN apt-get install -y cron

# Add app user (and drop to user)
RUN useradd --create-home --shell /bin/bash app
USER app
WORKDIR /app
